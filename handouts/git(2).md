---
marp: true
---

# git, bitbucket

## 11번가 2022년 개발 신입 역량육성과정

### (Deverloper's Beginning Course)

---

<!--
paginate: true
theme: default
size: 16:9
footer : 11번가 2022년 개발 신입 역량육성과정,  Wooyoung Choi, 2022
-->

## Previously..

- git의 원활한 사용을 위한 CLI shell, vim command
- 개발문서 작성을 위한 Markdown 작성법
- 원활한 git 사용 및 개발을 위한 환경 구성
- git의 구성요소와 process 실습
- commit convention 습관화
- repository 필수 요소의 작성

---

## Today, We will learn about..

- Branch 사용법
- Branch 전략: git flow
- 상황별 되돌리기
- jira로 프로젝트 관리하기
- 협업 하는 법: Forking Workflow

---
## Branch

![](https:wac-cdn.atlassian.com/dam/jcr:389059a7-214c-46a3-bc52-7781b4730301/hero.svg?cdnVersion=1389)

---

## Branch

- 분기점을 생성하여 독립적으로 코드를 변경할 수 있도록 도와주는 모델

`master`

```python
print('hello' + ' ' + 'world')
```

`develop`

```python
words = ['world', 'hello']
print(' '.join(words[:-1]))
```

---

## Branch(1)

Show available local branch
`$ git branch`

Show available remote branch
`$ git branch -r`

Show available All branch
`$ git branch -a`

---

## Branch(2)

Create branch
`$ git branch stem`

Checkout branch
`$ git checkout stem`

Create & Checkout branch
`$ git checkout -b new-stem`

make changes inside readme.md
`$ git commit -a -m 'edit readme.md'`
`$ git checkout master`

merge branch
`$ git merge stem`

---

## Branch(3)

delete branch
`$ git branch -D stem`

push with specified remote branch
`$ git push origin stem`

see the difference between two branches
`$ git diff master stem`

---

## Practice(1)

- Spiderman.md를 생성하고 다음의 정보를 배역을 맡은 배우별로 브랜치를 생성하여 이를 시각화 하세요.
- 완결된 브랜치는 master 브랜치로 merge 해야 합니다.
- 각 commit은 개봉연도 순서대로 존재해야 합니다.
- `AndrewGarfield` 브랜치는 master의 첫 commit 에서 `TobeyMaguire` 브랜치와 함께 시작해야 합니다.(리부트이므로..)
- Format

```text
# {Movie Name}
- Year:
- Name:
```

---

- `TobeyMaguire` branch

```text
# Spider-Man 1
- Year: 2002
- Name: Peter Benjamin Parker

# Spider-Man 2
- Year: 2004
- Name: Peter Benjamin Parker

# Spider-Man 3
- Year: 2007
- Name: Peter Benjamin Parker
```

---

- `AndrewGarfield` branch

```text
# Amazing Spider-Man 1
- Year: 2012
- Name: Peter Benjamin Parker

# Amazing Spider-Man 2
- Year: 2014
- Name: Peter Benjamin Parker
```

---

- `Tom Holland` branch

```text
# Captain America: Civil War
- Year: 2016
- Name: Peter Benjamin Parker

# Spider-Man: Home Coming
- Year: 2017
- Name: Peter Benjamin Parker

# Avengers: Infinity war
- Year: 2018
- Name: Peter Benjamin Parker

# Avengers: Endgame
- Year: 2019
- Name: Peter Benjamin Parker

# Spider-Man: Far From Home
- Year: 2019
- Name: Peter Benjamin Parker

# Spider-Man: No Way Home
- Year: 2022
- Name: Peter Benjamin Parker
```

---

## branching models

- git flow
  - (hotfix)- `master` -(release)- `develop` - feature
  - pros: 가장 많이 적용, 각 단계가 명확히 구분
  - cons: 복잡..
- github flow
  - `master` - feature
  - pros: 브랜치 모델 단순화, `master`의 모든 커밋은 deployable
  - cons: CI 의존성 높음. 누구 하나라도 실수했다간..(pull request로 방지)
- gitlab flow
  - `production` - `pre-production` - `master` - feature
  - pros: deploy, issue에 대한 대응이 가능하도록 보완
  - cons: git flow와 반대 (`master`-develop, `production`-master)

---

## git flow strategy

![height:450px](https://b.kisscc0.com/20180815/saq/kisscc0-github-branching-workflow-diagram-1492474981-svg-5b74286bb0f1a7.6096632915343391797248.png)

---

## use git flow easily!

[Link](https://danielkummer.github.io/git-flow-cheatsheet/index.ko_KR.html)

![](https://danielkummer.github.io/git-flow-cheatsheet/img/git-flow-commands.png)

---

## Practice(2)

- git flow 전략을 활용하여 어제 작성한 introduce.md를 index.html에 재작성하세요.

### Requirements

- develop 브랜치에서 다음 릴리즈를 위한 개발이 끝나야 합니다.
- head, body 등 section별 작업은 각각의 브랜치에서 작업되어야 합니다.
- css, js 작업 또한 각 브랜치를 소유합니다.(선택)
- [Semantic Web Elements](https://developer.mozilla.org/en-US/docs/Glossary/Semantics#Semantic_elements)를 적극 활용하세요.

---

## Revert Everything!

---

### Rename

- Worst
`$ mv server.py main.py` -> deleted, new file

- Best
`$ git mv server.py main.py` -> renamed

> 파일의 history를 남기기 위해서는 삭제 후 생성이 아닌 이름바꾸기로 추적

### [Undoing](https:asciinema.org/a/Fu27tl0y6RpHBPnTBHpPHh8St)

`$ git checkout -- .` or `$ git checkout -- {filename}`

---

### [Unstaging](https:asciinema.org/a/Y55mgQP0Yn8r6EEyBRSk0ysxX)

`$ git reset HEAD {filename}`

### Unstaging and Remove

`$ git rm -f {filename}`

---

### [Edit latest commit](https:asciinema.org/a/PplVwNAZND1lApmRLRo2q3jem)

`$ git commit --amend`

### [Edit prior commit](https:asciinema.org/a/03Kp8DnWV2mpBghfZ8MWAwN0q)

`$ git rebase -i <commit>`

#### [abort rebase](https:asciinema.org/a/ifWYyxutPfg04bTMrFA75KVms)

`$ git rebase --abort`

#### Complete rebase

`$ git rebase --continue`

---

### Reset Commit

#### Worst case: [Reset](https:asciinema.org/a/NvrtG3Nmf8cqa4gWlssjGmVEc)

ex) 직전 3개의 commit을 삭제한 후, remote에 강제 push

```shell
$ git reset --hard HEAD~3
$ git push -f origin <branch>
```

- 협업 시 다른 cloned repo에 존재하던 commit log로 인해 파일이 살아나거나, 과거 이력이 깔끔히 사라져 commit log tracking이 힘들어짐.
- solution: 잘못한 이력도 commit으로 박제하고 수정한 이력을 남기자!

---

#### Best case: [Revert](https:asciinema.org/a/bWGpPQM7rGMUlGrPSRc3OxjqS)

ex) 현재 HEAD에서 직전의 3개의 commit을 순서대로 거슬러 올라가 해당 내역에 대해 commit, push 수행

```
$ git revert --no-commit HEAD~3

$ git commit

$ git push origin <branch>
```

- 잘못하기 전 과거로 돌아가 최신을 유지하면서 되돌렸다는 이력을 commit으로 남겨 모든 팀원이 이 사항을 공유하고 주지시킬 수 있음.

---

## Link Bitbucket with Jira

---

## Collaborate with your teammates

---

### Forking workflow


---

### PM: Create a new repository

![height:400px](./img/collabo1.png)

---

### PM: Clone, create and push develop branch

![height:400px](./img/collabo2.png)

---

### PM: Check develop branch, Add dev1 

![height:300px](./img/collabo3.png)

![height:200px](./img/collabo4.png)

---

### dev1: Click +(Create), fork

![height:200px](./img/collabo5.png)

![height:200px](./img/collabo6.png)

---

### dev1: Fork complete

![height:300px](./img/collabo7.png)

![height:200px](./img/collabo8.png)

---

### dev1: Clone, git flow init

![height:150px](./img/collabo9.png)

![height:300px](./img/collabo10.png)

---

### dev1: Create feature branch, Do some work, finish feature branch

![height:250px](./img/collabo11.png)

![height:250px](./img/collabo12.png)

---

### dev1: Push to dev1:develop, Create a pull request

![height:200px](./img/collabo13.png)

![height:300px](./img/collabo14.png)

---

### dev1: Done!

![height:400px](./img/collabo15.png)

---

### PM: Review dev1's code

![height:300px](./img/collabo16.png)

---

### dev1: Push another commit to dev1:develop while pull request is open

![height:300px](./img/collabo17.png)

---

### PM: Approve pull request, Merge

![height:200px](./img/collabo18.png)

![height:200px](./img/collabo19.png)

---

### PM: Merge!!

![height:400px](./img/collabo20.png)

---

### dev2,dev3,devn, .. : Update develop branch

`$ git remote add pmorigin {PM's repo addr}`
`$ git fetch pmorigin develop` on develop branch
`$ git merge FETCH_HEAD`

---

## Practice(3)

- 3인이 팀이 되어 프로젝트 수행
- 아래의 과제 중 하나를 수행할 것
    1. [Pig the dice game](https://en.wikipedia.org/wiki/Pig_(dice_game))
    2. 인디언포커
    3. [What's my value?](https://www.transfermarkt.com/whatsMyValue) 게임(11번가 베스트 중 [가전/디지털](http://www.11st.co.kr/browsing/BestSeller.tmall?method=getBestSellerMain&cornerNo=10) 스크래핑 후 진행)
- Requirements
    - 타겟 플랫폼, 언어나 Framework는 팀 내 협의 후 결정
    - 서비스 기획 -> backlog 작성(trello) -> 개발 -> 평가 순으로 진행

---

### 2. 인디언 포커

- 아래의 인디언 포커 설명 문서를 읽고, 이를 구현 하시오
- https://en.wikipedia.org/wiki/Blind_man's_bluff_(poker)
- 덱 구성은 [1,2,3,4,5,6,7,8,9,10] * 2로 하되 모두 소진할 때 까지 운용한다.
- 컴퓨터는 오로지 받을 수만 있다.
- PVP일 경우, 베팅을 구현한다.(optional)
- 플레이어는 20개의 칩으로 시작하며, 칩을 모두 소진시키거나(승) 모두 소진할 경우(패) 게임이 종료된다.
- 게임 중 언제든지 :q를 입력하면 게임은 강제종료된다.
- 구현해야 할 일들을 issue에 등록하고 projects로 관리한다.

---

## Wrap it up!

- branch를 활용한 개발은 코드의 독립성을 보장한다.
- branching strategy 중 git flow 전략을 잘 활용하면 깔끔한 코드관리와 CI/CD에서도 도움이 될 수 있다.
- 모든 잘못된 작업은 push 하기 전까지 아무도 모르게 되돌릴 수 있다.
- Atlassian의 도구들을 잘 활용하면, 현재 프로젝트의 진행상황을 깔끔하게 관리할 수 있다.
- git flow 전략과 forking workflow를 활용하면 비동기적 분산형 저장소의 장점을 잘 살릴 수 있다.

<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,800" rel="stylesheet">
<link rel='stylesheet' href='//cdn.jsdelivr.net/npm/hack-font@3.3.0/build/web/hack-subset.css'>

<style>
h1,h2,h3,h4,h5,h6,
p,li, dd, table > * > * {
font-family: 'Nanum Gothic', Gothic;
}
span, pre {
font-family: 'Hack', monospace;
}
</style>