---
marp: true
---

# git, bitbucket

## 11번가 2022년 개발 신입 역량육성과정

### (Deverloper's Beginning Course)
---

<!--
paginate: true
theme: default
size: 16:9
footer : 11번가 2022년 개발 신입 역량육성과정,  Wooyoung Choi, 2022
-->

## 최우영

- Co-founder, CTO(disceptio)
- Solution Architect, Web Developer, Instructor
- Lang&Skills: Python, Golang, Julia, Node.js, Google tag manager ..

### Contacts

1. blog: https://ulgoon.github.io/
2. github: https://github.com/ulgoon/
3. email: me@ulgoon.com
4. discord: @ulgoon

---

## Goal

- git을 사용하기 위해 CLI Shell command와 Vim editor를 다룰 수 있다.
- 코드 관리를 위한 git의 정확한 사용법을 이해한다.
- git의 저장소 개념을 이해하고, 원격 저장소 서비스의 차이를 인식한다.
- git을 사용하면서 발생하는 다양한 상황을 해결할 수 있다.
- commit의 보편적인 작성법을 이해하고 이를 활용하여 commit을 작성할 수 있다.
- git의 branch model을 활용해 능숙하게 코드관리할 수 있다.
- git의 다양한 branch 전략을 이해하고 널리 사용되는 git flow 전략을 활용하여 프로젝트를 수행할 수 있다.
- jira와 bitbucket으로 프로젝트를 관리할 수 있다.
- git으로 타인과 협업하며, 다른 프로젝트에 기여할 수 있다.

---

## Before Start

### MacOS로 개발할 때 꼭 필요한 도구와 설정

1. [iTerm](https://iterm2.com/) => 기본 터미널의 대체재. tmux와 호환 Good
2. Xcode Command Line Tools(`$ xcode-select --install`) => 터미널 환경에서 다양한 도구 지원을 위해 필요한 도구
3. [OhMyZsh](https://ohmyz.sh/) => zsh Customize
4. [Homebrew](https://brew.sh/) => Missing Package Manager for MacOS

---

## Recap - Zsh Shell Command

```shell
$ cd Documents/
$ mkdir dev
$ cd ..
$ pwd

$ touch readme.md
$ mv readme.md bin/
$ cp readme.md bin/
$ mv readme.md ./README.txt
$ rm README.txt

$ rm -rf bin/
$ chmod 750 readme.md
$ cat readme.md
$ vi readme.md
```

---

## Practice(1)

### 다음의 절차를 수행하여 최종 디렉토리와 파일 구조를 완성하세요.

- `cli-practice/` 디렉토리를 생성하세요.
- 생성된 디렉토리에 `README.txt` 파일을 생성하세요.
- `bin/` 디렉토리를 생성하여 `README.txt`를 복사하여 `introduce.md`로 이름을 바꾸세요.

```text
~/Documents/dev
    - cli-practice/
        - bin/
            - introduce.md
        - README.txt
```

---

## Recap - Vim command

```text
h j k l - left, down, up, right
i - insert mode
v - visual mode
ESC - back to normal mode
d - delete
dd - delete a line
y - yank
yy - yank a line
p - paste
u - undo
a - append
A - append from end of line
o - open line(under)
O - open line(upper)
H - move to the top of the screen
L - move to the bottom of the screen
```

---

## Command mode

```text
:q - quit
:q! - quit discarding all changes
:w - write
:wq - write and quit
:{number} - jump to {number}th line.
```

---

### Bonus - Markdown

```text
<!-- 주석표기 -->

<!-- 제목 텍스트 -->
# h1
## h2
### h3
#### h4
##### h5
###### h6

<!-- 순서 없는 리스트(- * + 혼용 가능) -->
- Item1
    - Item1-1
        - Item1-1-1
* Item2
+ Item3

<!-- 순서 있는 리스트 -->
1. Item1
2. Item2
<!-- 하이퍼링크 -->
[링크 텍스트](링크 URL)

<!-- 이미지 -->
![대체 텍스트](이미지 URL)
```

---

```text
<!-- 강조 표기 -->
*Italic*
**Bold**
~Line Break~
_Single underscore_

<!-- 인용문(Blockquote) -->
> 인용할 문장

<!-- Code 입력(문장 내) -->
This is how `code` works.

<!-- Code 입력(블록) -->
` ``` `
def say_hello():
    return "hello"
` ```  `

<!-- 수평선 -->

Page 1

***
Page 2

-----
Page 3
```

---

## Practice(2)

### Vim 을 이용해 앞서 생성한 `introduce.md`에 Markdown 문법으로 간단한 자기소개 글을 작성하세요.

---

### Use Vim in real world!

- [vim advanture](https:vim-adventures.com/)
- [vimium](https:chrome.google.com/webstore/detail/vimium/dbepggeogbaibhgnhhndojpepiihcmeb?hl=en)
- [vs code vim extension](https:github.com/VSCodeVim/Vim)
- [intellij vim plugin](https:plugins.jetbrains.com/plugin/164-ideavim)

---

![height:200px](https://git-scm.com/images/logos/downloads/Git-Logo-2Color.png)

---

## VCS (Version Control System)

== SCM (Source Code Management)
< SCM (Software Configuration Management: 형상관리)

---

## chronicle of git

![](https:www.blogcdn.com/www.engadget.com/media/2009/10/linus-torvalds-gives-windows-7-a-big-thumbs-up.jpg)

---

## git by Linus Torvalds

- Linux Kernal을 만들기 위해 Subversion을 쓰다 화가 난 리누스 토발즈는 2주만에 git이라는 버전관리 시스템을 만듦
[git official repo](https:github.com/git/git)

---

## Characteristics of git

- 빠른속도, 단순한 구조
- 분산형 저장소 지원
- 비선형적 개발(수천개의 브랜치) 가능

---

## Pros of git

- **중간-발표자료_최종_진짜최종_15-4(교수님이 맘에들어함)_언제까지??_이걸로갑시다.ppt**

- 소스코드 주고받기 없이 동시작업이 가능해져 생산성이 증가
- 수정내용은 **commit** 단위로 관리, 배포 뿐 아니라 원하는 시점으로 **Checkout** 가능
- 새로운 기능 추가는 **Branch**로 개발하여 편안한 실험이 가능하며, 성공적으로 개발이 완료되면 **Merge**하여 반영
- 인터넷이 연결되지 않아도 개발할 수 있음

---

## git GUI Clients

- git GUI
- sourcetree
- kraken
- smartGit

---

## CLI first

- Source code를 Cloud Platform에서 사용할 경우, CLI 커맨드로 버전관리를 수행해야 합니다.
- CLI 커맨드로 git을 사용할 줄 알면, GUI 도구가 제공하는 기능에 대한 이해가 빠릅니다.
- 확인용도로 GUI를 참고하는 것은 Good^^

---

## git objects

- Blob: 파일 하나의 내용에 대한 정보
- Tree: Blob이나 subtree의 메타데이터(디렉토리 위치, 속성, 이름 등)
- Commit: 커밋 순간의 스냅샷

---

![height:400px](https://git-scm.com/book/en/v2/images/data-model-3.png)

- ref: [git internals - git objects](https:git-scm.com/book/en/v2/Git-Internals-Git-Objects)

---

## git Process Flow and Command

![height:500px](https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)

---

## Cloud Remote Repository Services

- Github: 비영리였던, Microsoft에 인수된 가장 유명한 서비스
- Bitbucket: Atlassian이 서비스. jira, confluence, trello 등의 부가도구와 유기적
- GitLab: GitLab이 서비스. 사설 서버 구성이 가능.

---

## Before Start

- git 설치 확인(`$ git -v`)
- git 환경설정

```shell
$ git config --global user.name "당신의유저네임"
$ git config --global user.email "당신의메일주소"
$ git config --global core.editor "vim"
$ git config --global core.pager "cat"
```
- lg alias 설정: [johanmeiring/gist:3002458](https://gist.github.com/johanmeiring/3002458)
- `$ git config --list` 로 정상 설정 확인
- 수정이 필요할 경우, `$ vi ~/.gitconfig` 에서 수정 가능

---

## Bitbucket - Sign up

[https:bitbucket.org](https:bitbucket.org)

---

## Bitbucket - New repo

[https:bitbucket.org/repo/create](https:bitbucket.org/repo/create)

---

## Bitbucket - New repo(1)

![height:400px](./img/newrepo1.png)

---

## Bitbucket - New repo(2)

![height:400px](./img/newrepo2.png)

---

## Bitbucket - New repo(3)

![height:400px](./img/newrepo3.png)

---

## Bitbucket - New repo(4)

![height:400px](./img/newrepo4.png)

---

## Bitbucket - New repo(5)

![height:300px](./img/newrepo5.png)

![height:100px](./img/newrepo6.png)

---

## First commit

```shell
$ touch introduce.md
$ vi introduce.md
$ git add introduce.md
$ git commit introduce.md
```

---

## Conventional Commits

ref: https://www.conventionalcommits.org/ko/v1.0.0/

1. commit의 제목은 commit을 설명하는 하나의 구나 절로 완성
2. importanceofcapitalize `Importance of Capitalize`
3. prefix 꼭 달기
    - feat: 기능 개발 관련
    - fix: 오류 개선 혹은 버그 패치
    - docs: 문서화 작업
    - test: test 관련
    - conf: 환경설정 관련
    - build: 빌드 관련
    - ci: Continuous Integration 관련

---

## Conventional Commits - example

> Commit Convention은 팀마다 다를 수 있으니 관련 문서를 참조할 것.

```text
feat: Add server.py
fix: Fix Typo server.py
docs: Add README.md, LICENSE
conf: Create .env, .gitignore, dockerfile
BREAKING CHANGE: Drop Support /api/v1
refactor: Refactor user classes
```

---

## commit 할 때 기억해야 할 것

- commit은 동작 가능한 최소단위로 자주 할 것.
- 해당 작업단위에 수행된 모든 파일 변화가 해당 commit에 포함되어야 함.
- 모두가 이해할 수 있는 log를 작성할 것.
- Open Source Contribution시 영어가 강제되지만, 그렇지 않을 경우 팀 내 사용 언어를 따라 쓸 것.
- 제목은 축약하여 쓰되(50자 이내), 내용은 문장형으로 작성하여 추가설명 할 것.
- 제목과 내용은 한 줄 띄워 분리할 것.
- 내용은 이 commit의 구성과 의도를 충실히 작성할 것.

---

## First Push

`$ git push origin master`

---

## Practice(3)

- 방금 생성한 repository에 다음 commit을 만족하도록 작성하여 push 하세요.
- requirements
    - 언어는 본인이 편한 언어로 작성할 것
    - 프로그래밍 언어 사용이 어려운 경우, .md 파일로 작성 가능

```text
- feat: Create adder.{py}
- feat: Add Feature - Return sum of 2 numbers
- fix: Rename Function add to adder
- docs: Create contribute.md to describe how to use these
- refactor: Create main.{py} to run on main function
```

---

## README.md

- 프로젝트와 Repository를 설명하는 책의 표지와 같은 문서
- 나와 동료, 이 repo의 사용자를 위한 문서

---

```text
# Project Name
Abstract your project in few lines.
see [project sample page](project link)

## Documentation

### Installation
To install,
`$ pip install sesame`
and run `$ python open_sesame.py`

### Supported Python versions
`>=3.6`

### More Information
- [API docs]()
- [Official website]()

### Contributing
Please see [CONTRIBUTING.md]()

### License
Sesame is Free software, and may be redistributed under the terms of specified in the [LICENSE]() file.
```

---

## .gitignore

`.gitignore`는 git이 파일을 추적할 때, 어떤 파일이나 폴더 등을 추적하지 않도록 명시하기 위해 작성하며, 해당 문서에 작성된 리스트는 수정사항이 발생해도 git이 무시하게 됩니다. 특정 파일 확장자를 무시하거나 이름에 패턴이 존재하는 경우, 또는 특정 디렉토리 아래의 모든 파일을 무시할 수 있습니다.

```
# 주석을 달기 위한 Hashtag
# MacOS Setup
.DS_Store
# Python cache files
.py[cdo]
# Important files
/Important
# AWS key
key.pem
```

---

## LICENSE

오픈소스 프로젝트에서 가장 중요한 License는 내가 만들 때에도, 배포할 때에도 가장 신경써야 하는 일 중 하나입니다.

가장 많이 사용하는 License는 다음과 같습니다.

- MIT License
    - MIT에서 만든 라이센스로, 모든 행동에 제약이 없으며, 저작권자는 소프트웨어와 관련한 책임에서 자유롭습니다.
- Apache License 2.0
    - Apache 재단이 만든 라이센스로, 특허권 관련 내용이 포함되어 있습니다.
- GNU General Public License v3.0
    - 가장 많이 알려져있으며, 의무사항(해당 라이센스가 적용된 소스코드 사용시 GPL을 따라야 함)이 존재합니다.

---

## git은 습관이 가장 중요!

- TIL(Today I Learned..) repository를 만들고 매일 학습하거나 얻은 지식을 정리
    - commit을 쌓아 commit 하는 습관도 기르고, 나중에 찾아보기 쉬움!
- Github blog
    - [hexo](https:hexo.io) + {username}.github.io repository로 정적 블로그를 만들어 정리하는 습관을 만들고 Markdown과 친해짐!

> TIL, Github blog는 github을 이용하여 개인적으로 관리 추천!

---

## Final Practice

### 다음의 프로젝트를 git을 활용하여 진행하시오

- 사용자의 이름(name)과 아이디(userName), 이메일(email)을 입력받아 masking 하여 출력
- 5자 이상일 경우, 첫 2개와 마지막 1개를 뺀 나머지를 masking
- email은 도메인을 masking 해서는 안됨
- 5자 미만일 경우, 처음과 마지막 글자만 masking
- lambda function 사용 금지
- 글자수는 유지
- CLI로 출력
- 각 기능은 function 단위로 정의되어 동작

---

## Wrap it up

- git의 원활한 사용을 위한 CLI shell, vim command
- 개발문서 작성을 위한 Markdown 작성법
- 원활한 git 사용 및 개발을 위한 환경 구성
- git의 구성요소와 process 실습
- commit convention 습관화
- repository 필수 요소의 작성

<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,800" rel="stylesheet">
<link rel='stylesheet' href='//cdn.jsdelivr.net/npm/hack-font@3.3.0/build/web/hack-subset.css'>

<style>
h1,h2,h3,h4,h5,h6,
p,li, dd, table > * > * {
font-family: 'Nanum Gothic', Gothic;
}
span, pre {
font-family: 'Hack', monospace;
}
</style>